Certainly! Below is an example of how you can create a textarea and update it with new images without losing the old ones. The textarea is defined outside of the async function, and it gets updated at the end of each image extraction run.

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Image Extractor</title>
</head>
<body>

<textarea id="imageUrls" rows="10" cols="50"></textarea>
<button id="extractImages">Extract Images</button>

<script>
// Create a Set to store unique image URLs
const imageSet = new Set();

// Get references to the textarea and button elements
const textarea = document.getElementById('imageUrls');
const extractButton = document.getElementById('extractImages');

// Function to simulate image extraction (replace this with your actual logic)
async function extractImages() {
    // Simulate an asynchronous operation (e.g., fetching data from an API)
    return new Promise((resolve) => {
        setTimeout(() => {
            const newImages = [
                'https://example.com/image1.jpg',
                'https://example.com/image2.jpg'
            ];
            resolve(newImages);
        }, 1000);
    });
}

// Function to update the textarea with all unique image URLs
function updateTextarea() {
    // Convert the Set to an array and join with newline characters for display
    const urlsArray = Array.from(imageSet);
    textarea.value = urlsArray.join('\n');
}

// Event listener for button click
extractButton.addEventListener('click', async () => {
    try {
        // Extract new images asynchronously
        const newImages = await extractImages();
        
        // Add each new image URL to the Set (duplicates will be ignored)
        newImages.forEach(url => imageSet.add(url));
        
        // Update the textarea with all unique image URLs
        updateTextarea();
        
    } catch (error) {
        console.error('Error extracting images:', error);
    }
});

</script>

</body>
</html>
```

### Explanation:
1. **HTML Structure**: 
   - A `textarea` element where extracted image URLs will be displayed.
   - A `button` element that triggers the extraction process when clicked.

2. **JavaScript Logic**:
   - A `Set` named `imageSet` is created to store unique image URLs.
   - References are obtained for both `textarea` and `button`.
   - An asynchronous function `extractImages()` simulates fetching or extracting images.
   - The function `updateTextarea()` updates the content of the textarea by converting the set of URLs into a string separated by newline characters.
   - An event listener on the button calls an anonymous async function that extracts images, adds them to the set, and updates the textarea.

This setup ensures that every time you click on "Extract Images", any newly extracted images are added without losing previously extracted ones, maintaining uniqueness through use of a JavaScript Set.

# Thu  4 Jul 16:54:33 CEST 2024 - create the textarea and set outside of the async function so new images are added to the set without losing old ones. update the textarea from the at the end of each image extraction run